﻿namespace FreightXChange.Data

open System.Diagnostics.CodeAnalysis
open System.Threading.Tasks
open System.Data.Entity

type IModelContext = interface
    abstract SmeSuppliers : IDbSet<SmeSupplier> with get, set
    abstract RetailerSuppliers : IDbSet<RetailerSupplier> with get, set
    abstract SaveChanges : unit -> int
    abstract SaveChangesAsync : unit -> Task<int>
end

[<ExcludeFromCodeCoverage>]
type ModelContext() =
    inherit DbContext("DefaultConnection")
 
    do Database.SetInitializer(new CreateDatabaseIfNotExists<ModelContext>())
 
    [<DefaultValue()>]
    val mutable smeSuppliers : IDbSet<SmeSupplier>
 
    [<DefaultValue()>]
    val mutable retailerSuppliers : IDbSet<RetailerSupplier>

    interface IModelContext with
        member this.SmeSuppliers with get() = this.smeSuppliers and set v = this.smeSuppliers <- v
        member this.RetailerSuppliers with get() = this.retailerSuppliers and set v = this.retailerSuppliers <- v
        member this.SaveChanges() =
            let context = this
            context.SaveChanges()
        member this.SaveChangesAsync() =
            let context = this
            context.SaveChangesAsync()