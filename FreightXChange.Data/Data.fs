﻿namespace FreightXChange.Data

open FreightXChange.Model
open System.ComponentModel.DataAnnotations
open System.Data.Entity.Spatial
open System

[<CLIMutable>]
type SmeSupplier = {
    [<Key>] Id : Guid
    [<Required; MaxLength(128)>] Name : string
    [<Required; MaxLength(128)>] Location : DbGeography
}

[<CLIMutable>]
type RetailerSupplier = {
    [<Key>] Id : Guid
    [<Required; MaxLength(128)>] Name : string
    [<Required; MaxLength(128)>] Location : DbGeography
}

type IGetConsumerQuotes = interface
    abstract GetConsumerQuote : Consignment[] -> Consumer -> ConsumerQuote[]
end