﻿namespace FreightXChange.Model
    open System

    type Latitude = Latitude of float
    type Longitude = Longitude of float
    type Location = { Latitude: Latitude; Longitude: Longitude }
    type SpatioTemporalEvent = { Time: DateTimeOffset; Location: Location }
    type TimeInterval = { Start: DateTimeOffset; End: DateTimeOffset }
    type LocationInterval = { Location: Location; Interval: TimeInterval }
    type CompanyName = CompanyName of string
    type Company = { CompanyName: CompanyName }
    type Username = Username of string
    type RegisteredUser = { Username: Username; CompanyName: CompanyName }
    type ConsolidationCentre = { Name: string; Location: Location }
    [<Measure>] type m
    type Volume = Volume of float<m^3> with
        static member (+) (Volume lhs, Volume rhs) =
            Volume (lhs + rhs)
        static member (-) (Volume lhs, Volume rhs) =
            if rhs > lhs then failwith "It is not possible to remove more than the original volume."
            Volume (lhs - rhs)
    [<Measure>] type kg
    type Mass = Mass of float<kg> with
        static member (+) (Mass lhs, Mass rhs) =
            Mass (lhs + rhs)
        static member (-) (Mass lhs, Mass rhs) =
            if rhs > lhs then failwith "It is not possible to remove more than the original mass."
            Mass (lhs - rhs)
    [<Measure>] type Pounds
    type Sterling = Sterling of decimal<Pounds> with
        static member (+) (Sterling lhs, Sterling rhs) =
            Sterling (lhs + rhs)
        static member (-) (Sterling lhs, Sterling rhs) =
            Sterling (lhs - rhs)
    type StorageSpace = { ConsolidationCentre: ConsolidationCentre; Volume: Volume } with
        static member (+) (lhs: StorageSpace, rhs: StorageSpace) =
            if (lhs.ConsolidationCentre <> rhs.ConsolidationCentre) then failwith "You cannot add storage space from a different consolidation centre."
            { ConsolidationCentre = lhs.ConsolidationCentre; Volume = lhs.Volume + rhs.Volume }
    type Brand = Brand of string
    type Supplier = { Company: Company }
    type Product = Fags | Booze | BogRoll | Stationery
    type BoxOfFags = { Mass: Mass; Volume: Volume; Supplier: Supplier; Brand: Brand }
    type CrateOfBooze = { Mass: Mass; Volume: Volume; Supplier: Supplier; Brand: Brand }
    type PacketOfBogRoll = { Mass: Mass; Volume: Volume; Supplier: Supplier; Brand: Brand }
    type BoxOfStationery = { Mass: Mass; Volume: Volume; Supplier: Supplier; Brand: Brand }
    type Quantity = Quantity of int with
        static member (*) (Quantity lhs, rhs: int) =
            Quantity (lhs * rhs)
    type BoxesOfFags = { Supplier: Supplier; BoxOfFags: BoxOfFags; Quantity: int } with
        static member (*) (lhs: BoxesOfFags, rhs: int) =
            { Supplier = lhs.Supplier; BoxOfFags = lhs.BoxOfFags; Quantity = lhs.Quantity * rhs }
    type CratesOfBooze = { CrateOfBooze: CrateOfBooze; Quantity: int } with
        static member (*) (lhs: CratesOfBooze, rhs: int) =
            { CrateOfBooze = lhs.CrateOfBooze; Quantity = lhs.Quantity * rhs }
    type PacketsOfBogRoll = { PacketOfBogRoll: PacketOfBogRoll; Quantity: int } with
        static member (*) (lhs: PacketsOfBogRoll, rhs: int) =
            { PacketOfBogRoll = lhs.PacketOfBogRoll; Quantity = lhs.Quantity * rhs }
    type BoxesOfStationery = { BoxOfStationery: BoxOfStationery; Quantity: int } with
        static member (*) (lhs: BoxesOfStationery, rhs: int) =
            { BoxOfStationery = lhs.BoxOfStationery; Quantity = lhs.Quantity * rhs }
    type Vehicle = Lgv | Hdv
    type Retailer = { Chain: Company; Location: Location }
    type Consumer = Retailer | SmallBusiness
    type SupplierUnit = BoxOfFags of BoxOfFags | CrateOfBooze of CrateOfBooze | PacketOfBogRoll of PacketOfBogRoll | BoxOfStationery of BoxOfStationery
    type Consignment = Fags of BoxesOfFags | Booze of CratesOfBooze | BogRoll of PacketsOfBogRoll | Stationery of BoxesOfStationery
    type SupplierQuote = { Product: Product; PricePerUnit: Sterling; SupplierUnit: SupplierUnit; ShippingCost: Sterling }
    type Delivery = { Consumer: Consumer; Cargo: Consignment[] }
    type ScheduledDelivery = { Delivery: Delivery; TimeInterval: TimeInterval }
    type CompletedDelivery = { Delivery: Delivery; Time: DateTimeOffset }
    type ConsumerQuote = { Product: Product; Supplier: Supplier; Delivery: ScheduledDelivery; ShippingCost: Sterling }
    type ConsolidationCentreAllocation = { Consumer: Consumer; StorageSpace: StorageSpace; TimeInterval: TimeInterval }
    type Journey = { ConsolidationCentre: ConsolidationCentre; Vehicle: Vehicle }
