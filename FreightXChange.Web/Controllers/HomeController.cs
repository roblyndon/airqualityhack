﻿using System;
using System.Linq;
using System.Web.Mvc;
using FreightXChange.Web.Models;

namespace FreightXChange.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Login", "Account");
            if (User.IsInRole("Sme") || User.Identity.Name.Contains("steve")) return RedirectToAction("SmallBusiness");
            if (User.IsInRole("Retailer") || User.Identity.Name.Contains("ken")) return RedirectToAction("Retailer");
            if (User.IsInRole("Supplier")) return RedirectToAction("Supplier");
            return View();
        }

        public ActionResult Supplier()
        {
            return View();
        }

        public ActionResult SmallBusiness()
        {
            var model = new ProductsAvailableViewModel
            {
                Action = "OrderSme",
                ConsumerType = "SME",
                Products = new[]
                {
                    new Product { Brand = "Andrex", Name = "Bog Roll", SupplierUnit = "Packets", PricePerUnit = 10.53m, ImageUri = "/Content/img/bogroll.jpg" },
                    new Product { Brand = "Velvet", Name = "Bog Roll", SupplierUnit = "Packets", PricePerUnit = 10.42m, ImageUri = "/Content/img/bogroll.jpg" },
                    new Product { Brand = "Izal", Name = "Bog Roll", SupplierUnit = "Packets", PricePerUnit = 5.43m, ImageUri = "/Content/img/bogroll.jpg" },
                    new Product { Brand = "Viking", Name = "Stationery", SupplierUnit = "Boxes", PricePerUnit = 17.43m, ImageUri = "/Content/img/stationery.jpg" },
                    new Product { Brand = "WHSmith", Name = "Stationery", SupplierUnit = "Boxes", PricePerUnit = 19.43m, ImageUri = "/Content/img/stationery.jpg" },
                    new Product { Brand = "Ryman", Name = "Stationery", SupplierUnit = "Boxes", PricePerUnit = 20.43m, ImageUri = "/Content/img/stationery.jpg" },
                }.ToList()
            };
            return View("Products", model);
        }

        public ActionResult Retailer()
        {
            var model = new ProductsAvailableViewModel
            {
                Action = "OrderRetailer",
                ConsumerType = "Retailer",
                Products = new[]
                {
                    new Product { Brand = "Marlboro", Name = "Fags", SupplierUnit = "Boxes", PricePerUnit = 25.53m, ImageUri = "/Content/img/fags.jpg" },
                    new Product { Brand = "Bensons", Name = "Fags", SupplierUnit = "Boxes", PricePerUnit = 25.23m, ImageUri = "/Content/img/fags.jpg" },
                    new Product { Brand = "Players", Name = "Fags", SupplierUnit = "Boxes", PricePerUnit = 24.41m, ImageUri = "/Content/img/fags.jpg" },
                    new Product { Brand = "Stella", Name = "Booze", SupplierUnit = "Crates", PricePerUnit = 0.43m, ImageUri = "/Content/img/booze.jpg" },
                    new Product { Brand = "Special Brew", Name = "Booze", SupplierUnit = "Crates", PricePerUnit = 0.43m, ImageUri = "/Content/img/booze.jpg" },
                    new Product { Brand = "Diamond White", Name = "Booze", SupplierUnit = "Crates", PricePerUnit = 0.43m, ImageUri = "/Content/img/booze.jpg" },
                }.ToList()
            };
            return View("Products", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [HttpGet]
        public ActionResult OrderRetailerReview()
        {
            var viewModel = new DeliveryReviewViewModel
            {
                Deliveries = new[]
                {
                    new Delivery { Time = "Monday 15th June 3:45pm", Quote = 249.99m, 
                        Items = new []
                        {
                            new CargoItem { Brand = "Players", Name = "Fags", SupplierUnit = "Boxes", Units = 50 },
                            new CargoItem { Brand = "Stella", Name = "Booze", SupplierUnit = "Packets", Units = 30 },
                        }}, 
                    new Delivery { Time = "Wednesday 15th June 8:50pm", Quote = 199.99m, 
                        Items = new []
                        {
                            new CargoItem { Brand = "Players", Name = "Fags", SupplierUnit = "Boxes", Units = 50 },
                            new CargoItem { Brand = "Stella", Name = "Booze", SupplierUnit = "Packets", Units = 30 },
                        }}
                }
            };
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult OrderSmeReview()
        {
            var viewModel = new DeliveryReviewViewModel
            {
                Deliveries = new[]
                {
                    new Delivery { Time = "Monday 15th June 3:45pm", Quote = 249.99m, 
                        Items = new []
                        {
                            new CargoItem { Brand = "Izal", Name = "Bog Roll", SupplierUnit = "Packets", Units = 50 },
                            new CargoItem { Brand = "Ryman", Name = "Stationery", SupplierUnit = "Boxes", Units = 30 },
                        }}, 
                    new Delivery { Time = "Wednesday 15th June 8:50pm", Quote = 199.99m, 
                        Items = new []
                        {
                            new CargoItem { Brand = "Izal", Name = "Bog Roll", SupplierUnit = "Packets", Units = 50 },
                            new CargoItem { Brand = "Ryman", Name = "Stationery", SupplierUnit = "Boxes", Units = 30 },
                        }}
                }
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult OrderSme()
        {
            return RedirectToAction("OrderSmeReview");
        }

        [HttpPost]
        public ActionResult OrderRetailer()
        {
            return RedirectToAction("OrderRetailerReview");
        }

        [HttpPost]
        public ActionResult ConfirmOrder()
        {
            return RedirectToAction("OrderConfirmed");
        }

        public ActionResult OrderConfirmed()
        {
            return View();
        }
    }
}