using System.Diagnostics.CodeAnalysis;
using System.Web.Optimization;
using FreightXChange.Web;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(FontAwesomeBundleConfig), "RegisterBundles")]

namespace FreightXChange.Web
{
    [ExcludeFromCodeCoverage]
    public class FontAwesomeBundleConfig
    {
        public static void RegisterBundles()
        {
            BundleTable.Bundles.Add(new StyleBundle("~/bundles/font-awesome/css")
                .Include("~/Content/font-awesome.css", new CssRewriteUrlTransform()));
        }
    }
}