using System.Web.Optimization;
using FreightXChange.Web;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(BootstrapBundleConfig), "RegisterBundles")]

namespace FreightXChange.Web
{
	public class BootstrapBundleConfig
	{
		public static void RegisterBundles()
		{
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));
			BundleTable.Bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include("~/Content/bootstrap.css", new CssRewriteUrlTransform()));
			BundleTable.Bundles.Add(new StyleBundle("~/bundles/bootstrap-theme/css").Include("~/Content/bootstrap-theme.css", new CssRewriteUrlTransform()));
		}
	}
}
