using System.Diagnostics.CodeAnalysis;
using System.Web.Optimization;
using FreightXChange.Web;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SuperAdminBundleConfig), "RegisterBundles")]

namespace FreightXChange.Web
{
    [ExcludeFromCodeCoverage]
    public class SuperAdminBundleConfig
    {
        public static void RegisterBundles()
        {
            BundleTable.Bundles.Add(new StyleBundle("~/bundles/super-admin/css")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/css/*.css", new CssRewriteUrlTransform()));
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/super-admin")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/sparkline.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/easypiechart.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/charts.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/maps/jvectormap.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/maps/world.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/icheck.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/scroll.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/calendar.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/feeds.js")
                .Include("~/Scripts/SuperAdmin-1-0-3/Template/1-0-3/js/functions.js"));
        }
    }
}