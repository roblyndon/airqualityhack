using System.Web.Optimization;
using FreightXChange.Web;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(JQueryBundleConfig), "RegisterBundles")]

namespace FreightXChange.Web
{
    public class JQueryBundleConfig
    {
        public static void RegisterBundles()
        {
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js", new CssRewriteUrlTransform()));
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui-{version}.js"));
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/jqueryeasing").Include("~/Scripts/jquery.easing.{version}.js"));
        }
    }
}