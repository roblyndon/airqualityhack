﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FreightXChange.Web.Startup))]
namespace FreightXChange.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
